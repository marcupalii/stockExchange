﻿
function AddToPortofolio(id) {
    $('#modal-edit-user').load("/Stock/AddToPortofolio/" + id);
    $('#modal-wrapper-edit-user').modal("show");
}


$(document).ready(function () {
    "use strict";
    var url = '/Stock/GetStocks';
    var pageScrollPos = 0;
    if ($.fn.DataTable.isDataTable("#table_id")) {
        $("#table_id").DataTable().destroy();
        $("#table_id").empty();
    }
    var table = $("#table_id").DataTable({
        "serverSide": true,
        "responsive": true,
        "ordering": true,
        "autoWidth": true,
        "processing": true,
        "searching": true,
        "footer": true,
        "fixedHeader": true,
        "lengthMenu": [10, 30, 50, 500],
        "pageLength": 10,
        "paging": true,
        "orderCellsTop": true,
        "ajax": {
            "url": url,
            "type": "POST",
            "datatype": "json",
            "dataSrc": function (json) {
                return json.data;
            },
            "complete": function (kqXHR, textStatus) {
                $($.fn.dataTable.tables(true)).DataTable()
                    .columns.adjust();
                $(".dataTables_scrollBody").scrollTop(pageScrollPos);
            }
        },
        rowId: "Id",
        "destroy": true,
        "retrieve": true,
        "dom": 'frt',
        "scrollY": "500px",
        "dom": 'Rlfrtip',
        "colReorder": {
            'allowReorder': true
        },
        "bInfo": true,
        "columns": [
            { title: "Id", name: "Id", data: "Id", "autowidth": true, "className": "show-none" },
            {
                title: 'Action', data: 'Id', "render": function (data, type, full) {
                    var result = "<span><i style='color:green;cursor:pointer' onclick='EditStock(\"" + data + "\")' class='fas fa-edit'></i>";
                    result += "  &nbsp;<a href='DeleteStock/?id=" + data + "' style='color:red;cursor:pointer;margin-left:7px;'><i class='fas fa-times'></i></a></span>";
                    result += "  &nbsp;<span><i style='color:green;cursor:pointer' onclick='AddToPortofolio(\"" + data + "\")' class='fas fa-plus'></i>";

                    return result;
                }
            },
            { title: "Stock Name", name: "Name", data: "Name", "autowidth": true },
            { title: "Stock Price", name: "Price", data: "Price", "autowidth": true },
            { title: "AvailableQuantity", name: "AvailableQuantity", data: "AvailableQuantity", "autowidth": true },
            { title: "RiskIndices", name: "RiskIndices", data: "RiskIndices", "autowidth": true },
        ],
        "fnInitComplete": function (settings, json) { },
        "preDrawCallback": function (settings) {
            pageScrollPos = $(".dataTables_scrollBody").scrollTop();
        },
    });

   
    var connection = new signalR.HubConnectionBuilder().withUrl("/stockChangesHub").build();
     
    connection.on("UpdateStockClient", function (user, message) {
        var data = JSON.parse(message);
   
        $(`tr#${user}`).children()[3].innerText = data.Price;
    
        //cc[3].nodeValue.text(data.Price);

        //var t = table.data();
        //var row = table.row(user);
        //var s = row.node();
        //var cell = table.cell({ row: user, column: 4 });
        //cell.data(data.Price).draw();

      //  table.row(user).invalidate().data(data).draw();
    });

    connection.start().then(function () {

    }).catch(function (err) {
        return console.error(err.toString());
    });

    

  
    $('#modal-title-edit-user').text('Add stock to Portofolio');
    $('#Id').select2();
    $('#button-save').on("click", function () {
        $('#form-AddToPortofolio').submit();
    });
   
  
});
