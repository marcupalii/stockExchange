
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using StockExchange.DataBase;
using StockExchange.DataBase.Entities;

namespace StockExchange
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews();
            services.AddMvc().AddJsonOptions(o =>
            {
                o.JsonSerializerOptions.PropertyNamingPolicy = null;
                o.JsonSerializerOptions.DictionaryKeyPolicy = null;
            });
            services.AddDbContext<DatabaseContext>(option =>
                option.UseSqlServer(Configuration.GetConnectionString("DbConnection")));
            services.AddIdentity<User, IdentityRole>().AddEntityFrameworkStores<DatabaseContext>();
            services.AddScoped<StockExchange.MiddleWare.IRole, StockExchange.MiddleWare.Role>();
            services.AddScoped<MiddleWare.IUser, MiddleWare.User>();
            services.AddScoped<MiddleWare.IEmailSender, MiddleWare.EmailSender>();
            services.AddScoped<MiddleWare.IStock, MiddleWare.Stock>();
            services.AddScoped<MiddleWare.IPortofolio, MiddleWare.Portofolio>();
            services.Configure<IdentityOptions>(options =>
            {
                options.Password.RequireDigit = false;
                options.Password.RequireLowercase = true;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireUppercase = false;
                options.Password.RequiredLength = 3;
                options.Password.RequiredUniqueChars = 1;
                options.User.AllowedUserNameCharacters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-._@+/";
            });
            services.AddSignalR();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, UserManager<StockExchange.DataBase.Entities.User> userManager, DatabaseContext db)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                new DataInitializer().InitializeDataAsync(userManager, db);
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }
            app.UseAuthentication();

            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();



            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");

                endpoints.MapControllerRoute(
                    name: "ResetLink",
                    pattern: "{controller=Account}/{action=RetypePassword}/{token1}&{token2}"
                    );
                endpoints.MapHub<StockExchange.SignalR.StockChangesHub>("/stockChangesHub");
            });
        }
    }
}
