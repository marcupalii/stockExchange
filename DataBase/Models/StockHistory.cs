﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StockExchange.DataBase.Models
{
    public class StockHistory
    {
        public Guid StockId { get; set; }
        public List<History> Values { get; set; }
        public StockHistory()
        {
            this.Values = new List<History>();
        }
    }
    public class History
    {
        public DayOfWeek Day { get; set; }
        public float Avg { get; set; }
        public History()
        {
            //this.Values = new List<float>();
        }
    }
}
