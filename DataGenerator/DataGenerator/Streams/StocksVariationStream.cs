﻿using Azure.Messaging.EventHubs;
using Azure.Messaging.EventHubs.Producer;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using StockExchange.DataBase;
using StockExchange.DataGenerator.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace StockExchange.DataGenerator.Streams
{
    public class StocksVariationStream
    {
        private string ConnectionString { get; set; }
        //private  string HubName { get; set; }

        private int ChancePriceModify;
        private float PriceValueMin;
        private float PriceValueMax;

        private int ChanceIncreaseQuantity;
        private float QuantityValueMin;
        private float QuantityValueMax;

        private  EventHubProducerClient client { get; set; }
        private  EventDataBatch batch { get; set; }
        private IConfiguration config;
    //    private DatabaseContext Database { get; set; }
        public StocksVariationStream()
        {
             config = new ConfigurationBuilder()
            .AddJsonFile("appsettings.json", true, true)
            .Build();

            ConnectionString = config["StocksVariationStream:ConectionString"];
           // HubName = config["StocksVariationStream:EventHub"];         

            ChancePriceModify = Int32.Parse(config["StocksVariationStream:ChancePriceModify"]);
            PriceValueMin = float.Parse(config["StocksVariationStream:PriceValueMin"]);
            PriceValueMax = float.Parse(config["StocksVariationStream:PriceValueMax"]);

            ChanceIncreaseQuantity = Int32.Parse(config["StocksVariationStream:ChanceIncreaseQuantity"]);
            QuantityValueMin = float.Parse(config["StocksVariationStream:QuantityValueMin"]);
            QuantityValueMax = float.Parse(config["StocksVariationStream:QuantityValueMax"]);

            client = new EventHubProducerClient(ConnectionString);
         //   Database = DatabaseContext.getDbInstance();
        }

        public void generateData()
        {
            var Database = DatabaseContext.getDbInstance();

            var randomPriceModify = new Random(DateTime.Now.Second);
            var randomPriceDirection = new Random(DateTime.Now.Second);
            var randomIncreaseQuantity = new Random(DateTime.Now.Second);
            
           
            bool modified = false;
            while (true)
            {
                var stocks = Database.Stock.ToList();
                foreach (var stock in stocks)
                {
                    var pretInit = stock.Price;
                    var totalQIntit = stock.ToTalQuantity;
                    modified = false;
                    var priceModify = randomPriceModify.Next(0, 5001);
                    if (priceModify < ChancePriceModify)
                    {
                        var valueProc = Extensions.Extensions.RandomNumber(PriceValueMin, PriceValueMax);
                        var value = (stock.Price * valueProc) / 100;
                        if (randomPriceDirection.Next(0, 101) < 50)  // price go down
                        {
                            stock.Price -= value;
                        }
                        else  // price go up
                        {
                            stock.Price += value;
                        }
                        modified = true;
                    }
                    var increaseQuantity = randomIncreaseQuantity.Next(0, 5001);
                    if (increaseQuantity < ChanceIncreaseQuantity)
                    {
                        var valueProc = Extensions.Extensions.RandomNumber(QuantityValueMin, QuantityValueMax);
                        var value = (stock.ToTalQuantity * valueProc) / 100;
                        stock.ToTalQuantity += value;
                        modified = true;
                    }

                    if (modified)
                    {
                        var stock_ = JsonConvert.SerializeObject(stock);
                        Console.WriteLine($"Stock modified [{stock.Name}], pret init: [{pretInit}], pret cur: [{stock.Price}], cant init: [{totalQIntit}], cant curr: [{stock.ToTalQuantity}]");

                        batch = client.CreateBatchAsync().Result;
                        batch.TryAdd(new EventData(Encoding.UTF8.GetBytes(stock_)));
                        client.SendAsync(batch).Wait();
                    }
                }
            }
         
         

        }
        public async Task closeConn()
        {
            await client.CloseAsync();
        }
    }
}
