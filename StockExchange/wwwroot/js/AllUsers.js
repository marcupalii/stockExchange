﻿
function EditUser(id) {
    $('#modal-edit-user').load("EditUser/" + id);
    $('#modal-wrapper-edit-user').modal("show");
}
function loadDataTables(url) {
    let table = "#table_id";
    var pageScrollPos = 0;
    if ($.fn.DataTable.isDataTable(table)) {
        $(table).DataTable().destroy();
        $(table).empty();
    }
    $(table).DataTable({
        "serverSide": true,
        "responsive": true,
        "ordering": true,
        "autoWidth": true,
        "processing": true,
        "searching": true,
        "footer": true,
        "fixedHeader": true,
        "lengthMenu": [10, 30, 50, 500],
        "pageLength": 10,
        "paging": true,
        "orderCellsTop": true,
        "ajax": {
            "url": url,
            "type": "POST",
            "datatype": "json",
            "dataSrc": function (json) {
                return json.data;
            },
            "complete": function (kqXHR, textStatus) {
                $($.fn.dataTable.tables(true)).DataTable()
                    .columns.adjust();
                $(".dataTables_scrollBody").scrollTop(pageScrollPos);
            }
        },
        "destroy": true,
        "retrieve": true,
        "dom": 'frt',
        "scrollY": "500px",
        "dom": 'Rlfrtip',
        "colReorder": {
            'allowReorder': true
        },
        "bInfo": true,
        "columns": [
            {
                title: 'Action', data: 'Id', "render": function (data, type, full) {
                    var result = "<span><i style='color:green;cursor:pointer' onclick='EditUser(\"" + data + "\")' class='fas fa-edit'></i>";
                    result += "  &nbsp;<a href='DeleteUser/?id=" + data + "' style='color:red;cursor:pointer;margin-left:7px;'><i class='fas fa-times'></i></a></span>";
                    return result;
                }
            },
            { title: "First name", name: "FirstName", data: "FirstName", "autowidth": true },
            { title: "Last name", name: "LastName", data: "LastName", "autowidth": true },
            { title: "Email", name: "Email", data: "Email", "autowidth": true },
            { title: "Address", name: "Address", data: "Address", "autowidth": true },
            { title: "Active", name: "Active", data: "Active", "autowidth": true },
            { title: "Roles", name: "Roles", data: "Roles", "autowidth": true },
        ],
        "fnInitComplete": function (settings, json) { },
        "preDrawCallback": function (settings) {
            pageScrollPos = $(".dataTables_scrollBody").scrollTop();
        },
    });

}

$(document).ready(function () {
    loadDataTables('/Users/GetUsers');
});
