﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using StockExchange.DataBase.Entities;
using System.Collections.Generic;

namespace StockExchange.DataBase
{
    public class DatabaseContext : IdentityDbContext
    {
        public DatabaseContext(DbContextOptions options) : base(options)
        {
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseLazyLoadingProxies();
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<IdentityUser>()
                .HasKey(e => e.Id);

            modelBuilder.Entity<IdentityUser>()
                .Property(e => e.Id)
                .ValueGeneratedOnAdd();

            modelBuilder.Entity<IdentityRole>()
                .HasKey(e => e.Id);

            modelBuilder.Entity<IdentityRole>()
                .Property(e => e.Id)
                .ValueGeneratedOnAdd();

            modelBuilder.Entity<IdentityUser>(entity =>
            {
                entity.ToTable(name: "User");
            });

            modelBuilder.Entity<IdentityRole>(entity =>
            {
                entity.ToTable(name: "Role");
            });
            modelBuilder.Entity<IdentityUserRole<string>>(entity =>
            {
                entity.ToTable("UserRoles");
            });

            modelBuilder.Entity<IdentityUserClaim<string>>(entity =>
            {
                entity.ToTable("UserClaims");
            });

            modelBuilder.Entity<IdentityUserLogin<string>>(entity =>
            {
                entity.ToTable("UserLogins");
            });

            modelBuilder.Entity<IdentityRoleClaim<string>>(entity =>
            {
                entity.ToTable("RoleClaims");
            });

            modelBuilder.Entity<IdentityUserToken<string>>(entity =>
            {
                entity.ToTable("UserTokens");
            });

        }
        public static DatabaseContext getDbInstance()
        {
            IConfiguration config = new ConfigurationBuilder()
           .AddJsonFile("appsettings.json", true, true)
           .Build();

            var connectionString = config["ConnectionStrings:DbConnection"];


            var builder = new DbContextOptionsBuilder();
            builder.UseSqlServer(connectionString)
                   .UseLazyLoadingProxies(true);

            return new DatabaseContext(builder.Options);
        }
        public static UserManager<IdentityUser> getUserManager()
        {
            var passWordOptions = new PasswordOptions()
            {
                RequireDigit = false,
                RequireLowercase = true,
                RequireNonAlphanumeric = false,
                RequireUppercase = false,
                RequiredLength = 3,
                RequiredUniqueChars = 1,
            };
            var userOptions = new UserOptions()
            {
                AllowedUserNameCharacters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-._@+/"
            };
            var identityOptions = new IdentityOptions()
            {
                Password = passWordOptions,
                User = userOptions
            };

            var dbContext = DatabaseContext.getDbInstance();


            var userStore = new UserStore<IdentityUser>(dbContext);
            IPasswordHasher<IdentityUser> hasher = new PasswordHasher<IdentityUser>();
            var validator = new UserValidator<IdentityUser>();
            var validators = new List<UserValidator<IdentityUser>> { validator };

   
            ILogger<UserManager<IdentityUser>> logger = new Logger<UserManager<IdentityUser>>((ILoggerFactory)new LoggerFactory());
            var userManager = new UserManager<IdentityUser>(userStore, null, hasher, validators, null, null, null, null, logger);

            // Set-up token providers.
            IUserTwoFactorTokenProvider<IdentityUser> tokenProvider = new EmailTokenProvider<IdentityUser>();
            userManager.RegisterTokenProvider("Default", tokenProvider);
            IUserTwoFactorTokenProvider<IdentityUser> phoneTokenProvider = new PhoneNumberTokenProvider<IdentityUser>();
            userManager.RegisterTokenProvider("PhoneTokenProvider", phoneTokenProvider);

            userManager.Options = identityOptions;

            return userManager;
            //////////////////////////////////

           // var manager = new UserManager<IdentityUser>(store, identityOptions);
        }
        public DbSet<User> User { get; set; }
        public DbSet<Stock> Stock { get; set; }
        public DbSet<Trade> Trade { get; set; }
        public DbSet<Portofolio> Portofolio { get; set; }
        public DbSet<PortofolioItems> PortofolioItems { get; set; }
    }
}
