﻿using Microsoft.AspNetCore.Identity;
using StockExchange.DataBase;
using System;
using System.Collections.Generic;
using System.Linq;

namespace StockExchange
{
    internal class DataInitializer
    {
        private UserManager<StockExchange.DataBase.Entities.User> userManager;
        private DatabaseContext db;
        public DataInitializer()
        {
        }
        private void CreateRoles()
        {
            db.Roles.AddRange(new IdentityRole()
            {
                Name = "admin"
            }, new IdentityRole()
            {
                Name = "user"
            });

            db.SaveChanges();
        }
        private void CreateStock()
        {
            var names = new List<string>() { "Joystick Joy", "GameFinity", "Gamerbux", "GameRaider","NewConsole", "Juego","Ogame","Mappy","JoyPad","Emuler","Adibou", "Dofus","Cruis","PSTY","Pepsi","Adacku","Aonuma","LogicGamers","Neogeo","Lexus","Ferano" };
            foreach(var name in names)
            {
                var stock3 = new StockExchange.DataBase.Entities.Stock()
                {
                    Name = name,
                    Price = 420,
                    RiskIndices = 0.1f,
                    ToTalQuantity = 2340000,
                    AvailableQuantity = 2340000,

                };
                db.Stock.Add(stock3);
                db.SaveChanges();
            }
           
        }
        private void CreateUsers()
        {
            for (int i = 1; i < 5; i++)
            {
                var userRole = db.Roles.FirstOrDefault(x => x.Name == "user");
                var newUser = new StockExchange.DataBase.Entities.User()
                {
                    Address = "Address",
                    Email = $"user{i}@gmail.com",
                    EmailConfirmed = false,
                    LastName = $"LastName{i}",
                    FirstName = $"FirstName{i}",
                    UserName = $"user{i}@gmail.com",
                };

                var result = userManager.CreateAsync(newUser, $"pass{i}").Result;

                if (result.Succeeded)
                {
                    db.Entry(newUser).Reload();
                    var roleAssigned = new IdentityUserRole<string>()
                    {
                        UserId = newUser.Id,
                        RoleId = userRole.Id,
                    };
                    db.UserRoles.Add(roleAssigned);
                    db.SaveChanges();
                }
            }


            var userRol = db.Roles.FirstOrDefault(x => x.Name == "user");
            var adminRol = db.Roles.FirstOrDefault(x => x.Name == "admin");
            var user = new StockExchange.DataBase.Entities.User()
            {
                Address = "Address",
                Email = "marcupalii@gmail.com",
                EmailConfirmed = false,
                FirstName = "admin",
                LastName = "admin",
                UserName = "marcupalii@gmail.com",
            };

            var res = userManager.CreateAsync(user, "admin").Result;

            if (res.Succeeded)
            {
                db.Entry(user).Reload();
                var roleAssigned1 = new IdentityUserRole<string>()
                {
                    UserId = user.Id,
                    RoleId = userRol.Id
                };
                var roleAssigned2 = new IdentityUserRole<string>()
                {
                    UserId = user.Id,
                    RoleId = adminRol.Id
                };
                db.UserRoles.AddRange(roleAssigned1, roleAssigned2);
                db.SaveChanges();
            }

        }
   
        public void InitializeDataAsync(UserManager<StockExchange.DataBase.Entities.User> userManager, StockExchange.DataBase.DatabaseContext db)
        {
            this.userManager = userManager;
            this.db = db;
            try
            {
                if (db.UserRoles.Count() == 0)
                {
                    CreateRoles();
                }
                if (db.User.Count() == 0)
                {
                    CreateUsers();
                }
                if (db.Stock.Count() == 0)
                {
                    CreateStock();
                }

            }
            catch (Exception exception)
            {
            }
        }

    }
}
