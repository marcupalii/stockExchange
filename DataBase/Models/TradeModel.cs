﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using static Extensions.Extensions;

namespace StockExchange.DataBase.Models
{
    public class TradeModel
    {
        
        [Display(Name = "Stock Name: ")]
        public string StockName { get; set; }

        [Required]
        public Guid StockId { get; set; }

        [Required]
        public Guid PortofolioId { get; set; }

        [Required]
        public Guid Id { get; set; }

        [Required]
        [Display(Name = "Stock Price: ")]
        public float PriceStock { get; set; }

        [Required]
        [Display(Name = "Choose Quantity: ")]
        public int Quantity { get; set; }

        [Required]
        [Display(Name = "Total Price: ")]
        public float TotalPrice { get; set; }

        [Required]
        [Display(Name = "Quantity Own: ")]
        public int QuantityOwn { get; set; }

        [Required]
        [Display(Name = "Value for Own Quantity: ")]
        public int Sum { get; set; }

        [Required]
        [Display(Name = "Trade Type: ")]
        public int OperationType { get; set; }
        
        public TradeModel()
        {
            this.Id = Guid.NewGuid();
        }
    }
}
