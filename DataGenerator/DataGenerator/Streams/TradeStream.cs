﻿using Azure.Messaging.EventHubs;
using Azure.Messaging.EventHubs.Producer;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using StockExchange.DataBase;
using StockExchange.DataGenerator.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using static Extensions.Extensions;

namespace StockExchange.DataGenerator.Streams
{
    public class TradeStream
    {
        private string ConnectionString { get; set; }
       // private  string HubName { get; set; }


        private float StockChanceChange { get; set; }
        private float QuantityValueMin { get; set; }
        private float QuantityValueMax { get; set; }
        private float OperationTypeSellProc { get; set; }


        private  EventHubProducerClient client { get; set; }
        private  EventDataBatch batch { get; set; }

        private IConfiguration config;
        // private DatabaseContext Database { get; set; }
        public TradeStream()
        {
             config = new ConfigurationBuilder()
            .AddJsonFile("appsettings.json", true, true)
            .Build();

            ConnectionString = config["TradeStream:ConectionString"];
           // HubName = config["TradeStream:EventHub"];

            
            StockChanceChange = float.Parse(config["TradeStream:StockChanceChange"]);
            QuantityValueMin = float.Parse(config["TradeStream:QuantityValueMin"]);
            QuantityValueMax = float.Parse(config["TradeStream:QuantityValueMax"]);
            OperationTypeSellProc = float.Parse(config["TradeStream:OperationTypeSellProc"]);


            client = new EventHubProducerClient(ConnectionString);
            //Database = DatabaseContext.getDbInstance();
        }

   

        public void generateData()
        {
            var Database = DatabaseContext.getDbInstance();
            var randomStockChance = new Random(DateTime.Now.Second);
            var randomType = new Random(DateTime.Now.Second);

            while (true)
            {
                var stocks = Database.Stock.ToList();

                foreach(var stock in stocks)
                {
                    if (randomStockChance.Next(0, 5001) < StockChanceChange) // trade stock
                    {
                        var trade = new StockExchange.DataGenerator.Models.Trade()
                        {
                            Price = stock.Price,
                            StockId = stock.Id,
                            Date = DateTime.Now,
                        };


                        if (randomType.Next(0, 5001) < OperationTypeSellProc)
                        {
                            trade.Type = OperationType.Sell;
                        }
                        else
                        {
                            trade.Type = OperationType.Buy;
                        }

                        var valueProc = Extensions.Extensions.RandomNumber(QuantityValueMin, QuantityValueMax);
                        trade.Quantity = (stock.ToTalQuantity * valueProc) / 100;


                        var userCount = Database.User.Count();
                        var indexUser = userCount == 1? 0: randomType.Next(0, userCount-2);
                        var user = Database.User.Skip(indexUser).Take(1).FirstOrDefault();
                        trade.UserId = Guid.Parse(user.Id);

                        var trade_ = JsonConvert.SerializeObject(trade);
                        Console.WriteLine($"Trade stock: [{stock.Name}], by user: [{user.Email}], price: [{trade.Price}], type: [{trade.Type}], qty: [{trade.Quantity}]");

                        batch = client.CreateBatchAsync().Result;
                        batch.TryAdd(new EventData(Encoding.UTF8.GetBytes(trade_)));
                        client.SendAsync(batch).Wait();
                    }
              
                }
            }

        }
        public async Task closeConn()
        {
            await client.CloseAsync();
        }
    }
}
