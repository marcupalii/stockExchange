﻿
using System;
using System.Runtime.Serialization;

namespace StockExchange.DataGenerator.Models
{
    
    [DataContract]
    public class User
    {
        [DataMember]
        public Guid Id {get; set; }
        
        [DataMember]
        public String Email {get; set; }
        
        [DataMember]
        public String FirstName{get; set; }
        [DataMember]
        public String LastName {get; set; }


    }
}
