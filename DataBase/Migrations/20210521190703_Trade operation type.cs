﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace StockExchange.DataBase.Migrations
{
    public partial class Tradeoperationtype : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "OperationType",
                table: "Trade",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "OperationType",
                table: "Trade");
        }
    }
}
