﻿namespace Jobs
{
    using System;

    using System.IO;
    using System.Threading;
    using System.Threading.Tasks;
    using AzureServices;
    using Microsoft.AspNetCore.SignalR.Client;
    using Microsoft.Azure.WebJobs;
    using Microsoft.Azure.WebJobs.Extensions.Timers;

    using Newtonsoft.Json;
    using StockExchange.DataBase.Entities;

   
    public class Functions
    {

        private static readonly object _hubLock = new object();
        private static HubConnection _hub;
        private static HubConnection hub
        {
            get
            {
                if (_hub == null)
                {
                    lock (_hubLock)
                    {
                        if (_hub == null)
                        {
                            _hub = new HubConnectionBuilder()
                               .WithUrl(new Uri("http://localhost:56620/stockChangesHub"))
                               .WithAutomaticReconnect()
                               .Build();

                             _hub.StartAsync().Wait();
                          
                        }
                    }
                }
                return _hub;
            }
        }

        private static readonly object _StockLock = new object();
        private static StockExchange.MiddleWare.Stock _StockService;
        private static StockExchange.MiddleWare.Stock StockService
        {
            get
            {
                if (_StockService == null)
                {
                    lock (_StockLock)
                    {
                        if (_StockService == null)
                        {
                            _StockService = new StockExchange.MiddleWare.Stock();
                        }
                    }
                }
                return _StockService;
            }
        }

        [Singleton(Mode = SingletonMode.Function)]
        public static async Task StockVariantionJob([EventHubTrigger("StockvariantionsOut", Connection = "HubName")] Stock stock)
        {
            try
            {
                var stock_ = JsonConvert.SerializeObject(stock);

                var _hub = new HubConnectionBuilder()
                        .WithUrl(new Uri("http://localhost:56620/stockChangesHub"))
                        .WithAutomaticReconnect()
                        .Build();

                await _hub.StartAsync();

                await _hub.SendCoreAsync("UpdateStock", new object[] { stock.Id.ToString(), stock_ });

                StockService.Update(stock);
            }
            catch (Exception ex)
            {
                var ss = ex;
            }
        }
        [Singleton(Mode = SingletonMode.Function)]
        public static void TradeProcessing([TimerTrigger("0 * * * * *", RunOnStartup = true)] TimerInfo timerInfo)
        {
            try
            {
                var blobService = new StorageBlobService();
                blobService.readTrades();
            }
            catch (Exception ex)
            {
               
            }
        }

    }
}
