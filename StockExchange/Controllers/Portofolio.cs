﻿using System;
using System.Linq;
using System.Threading.Tasks;
using StockExchange.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using StockExchange.MiddleWare;
using StockExchange.DataBase.Models;
using static Extensions.Extensions;
using System.Collections.Generic;

namespace StockExchange.Controllers
{
    [Authorize]
    public class PortofolioController : Controller
    {
        private Microsoft.Extensions.Logging.ILogger logger;
        private readonly UserManager<StockExchange.DataBase.Entities.User> userManager;

        private readonly IPortofolio portofolioService;
        private readonly IStock stockService;
        private readonly IEmailSender emailSender;
        private readonly StockExchange.DataBase.DatabaseContext db;
        public PortofolioController(ILogger<AccountController> logger, UserManager<StockExchange.DataBase.Entities.User> userManager, IStock Stock, IPortofolio Portofolio, IEmailSender emailSender, StockExchange.DataBase.DatabaseContext db)
        {
            this.logger = logger;
            this.userManager = userManager;
            this.portofolioService = Portofolio;
            this.stockService = Stock;
            this.emailSender = emailSender;
            this.db = db;
        }

        [HttpGet]
        [Route("Portofolio/Index/{topicId?}")]
        public async Task<IActionResult> Index([FromRoute] Guid? topicId)
        {
            var currentUser = userManager.GetUserAsync(HttpContext.User).Result;
            var portofolios = this.portofolioService.getUserPortofoliosWithStock(Guid.Parse(currentUser.Id));
            ViewBag.Portofolios = portofolios.Select(x => new IdDisplayItem() { Id = x.Id, Display = x.Name }).ToList();

            var portofolio = this.portofolioService.GetByIdWithStock(topicId);
            return View("Portofolio", portofolio);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> NewPortofolio(StockExchange.DataBase.Models.NewPortofolio model)
        {
            if (ModelState.IsValid)
            {

                this.portofolioService.AddPortofolio(model);
                return RedirectToAction("Index", "Portofolio");
            }
            else
            {
                return View("NewPortofolio", model);
            }

        }

        [HttpGet]
        public IActionResult NewPortofolio()
        {
            var currentUser = userManager.GetUserAsync(HttpContext.User).Result;
            var portofolio = new StockExchange.DataBase.Models.NewPortofolio() { Id = Guid.NewGuid(), UserId = Guid.Parse(currentUser.Id) };
            return PartialView("NewPortofolio", portofolio);
        }


        [HttpGet]
        [Route("Portofolio/{portofolioId?}/Trade/{id?}/{price?}")]
        public async Task<IActionResult> Trade([FromRoute] Guid? portofolioId, [FromRoute] Guid? id, [FromRoute] float? price)
        {
            var currentUser = userManager.GetUserAsync(HttpContext.User).Result;
            var stock = stockService.GetById(id);
            var portofolio = portofolioService.GetByIdWithStock(portofolioId.Value);
            var portofolioItem = portofolio.PortofolioItems.FirstOrDefault(x => x.Stock.Id == id);
            ViewBag.OperationTypes = new List<object>(){
                new { Id= (int)OperationType.Sell, Display = OperationType.Sell },
                new { Id= (int)OperationType.Buy, Display = OperationType.Buy }
            };


            var trade = new TradeModel()
            {
                StockName = stock.Name,
                StockId = stock.Id,
                PriceStock = price.Value,
                PortofolioId = portofolioId.Value,
                QuantityOwn = (int)portofolioItem.QuantityOwn,
                Sum =(int) portofolioItem.Money
            };
            return PartialView("_Trade", trade);
        }
        [HttpPost]
        [Route("Portofolio/Trade")]
        public async Task<IActionResult> Trade(TradeModel model)
        {
         
            try
            {
                if (ModelState.IsValid)
                {
                    var currentUser = userManager.GetUserAsync(HttpContext.User).Result;
                    stockService.Add(model,Guid.Parse(currentUser.Id));
                 
                    return RedirectToAction("Index", "Portofolio", new { topicId = model.PortofolioId });
                }
                else
                    return PartialView("_Trade", model);
            }
            catch (Exception ex)
            {
                return RedirectToAction("Index", "Portofolio", new { topicId = model.PortofolioId });
            }
        }


        [Route("/Portofolio/StocksHistory/{portofolioId?}/")]
        [HttpGet]
        public IActionResult StocksHistory([FromRoute] Guid? portofolioId)
        {
            var portofolio = portofolioService.GetByIdWithStock(portofolioId.Value);
            var stockIds = portofolio.PortofolioItems.Select(x => x.Stock.Id).ToList();

            var res = stockService.getStockHistoryById(stockIds);

            return Json(res);
        }
    }

}