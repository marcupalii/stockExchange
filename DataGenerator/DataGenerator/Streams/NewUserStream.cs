﻿using Azure.Messaging.EventHubs;
using Azure.Messaging.EventHubs.Producer;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using StockExchange.DataBase;
using StockExchange.DataGenerator.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace StockExchange.DataGenerator.Streams
{
    public class NewUserStream
    {
        private string ConnectionString { get; set; }
      //  private  string HubName { get; set; }
        private TimeSpan Timestamp { get; set; }
        private int Chance;
        private float LastAdded { get; set; }

        private  EventHubProducerClient client { get; set; }
        private  EventDataBatch batch { get; set; }

        private int CountData { get; set; }=0;
        private UserManager<IdentityUser> UserManager { get; set; }
        private IConfiguration config;

        public NewUserStream()
        {
             config = new ConfigurationBuilder()
            .AddJsonFile("appsettings.json", true, true)
            .Build();

            ConnectionString = config["NewUserStream:ConectionString"];
          //  HubName = config["NewUserStream:EventHub"];
            Timestamp = TimeSpan.Parse(config["NewUserStream:Timestamp"]);
            
            Chance = Int32.Parse(config["NewUserStream:Chance"]);
            LastAdded = float.Parse(config["NewUserStream:LastAdded"]);


            client = new EventHubProducerClient(ConnectionString);
            UserManager = DatabaseContext.getUserManager();
       
        }
        private StockExchange.DataBase.Entities.User AddUser(StockExchange.DataBase.Entities.User user, string pass)
        {
            var Database = DatabaseContext.getDbInstance();
            var userRole = Database.Roles.FirstOrDefault(x => x.Name == "user");


            var result = UserManager.CreateAsync(user, pass).Result;

            if (result.Succeeded)
            {
                Database.Entry(user).Reload();
                var roleAssigned = new IdentityUserRole<string>()
                {
                    UserId = user.Id,
                    RoleId = userRole.Id,
                };
                Database.UserRoles.Add(roleAssigned);
                Database.SaveChanges();
            }
            return user;
        }  

        public void generateData()
        {
            var random = new Random((int)LastAdded);
            var start = DateTime.Now;
            while (true)
            {
                var current = DateTime.Now;
                if (current-start < Timestamp)
                {
                    continue;
                }
                start = current;

                var number = random.Next(0, 101);
                if (number < Chance)
                {
                    var newUser = new StockExchange.DataBase.Entities.User()
                    {
                        Address = "Address",
                        Email = $"user{LastAdded}@gmail.com",
                        EmailConfirmed = false,
                        LastName = $"LastName{LastAdded}",
                        FirstName = $"FirstName{LastAdded}",
                        UserName = $"user{LastAdded}@gmail.com",
                    };
                    newUser = AddUser(newUser, $"pass{LastAdded}");

                    LastAdded++;
                    Extensions.Extensions.AddOrUpdateAppSetting<string>("NewUserStream:LastAdded", LastAdded.ToString());
                  //  config["NewUserStream:LastAdded"] = LastAdded.ToString();
                    CountData++;

                    var user_ = JsonConvert.SerializeObject(newUser);
                    Console.WriteLine($"Added user {newUser.Email}, date: {DateTime.Now}");

                    batch =  client.CreateBatchAsync().Result;
                    batch.TryAdd(new EventData(Encoding.UTF8.GetBytes(user_)));
                    client.SendAsync(batch).Wait();  
                }
            }
        

            //var aTimer = new System.Timers.Timer(Timestamp.TotalMilliseconds);

            //aTimer.Elapsed += (sender, e) => OnTimedEvent(sender, e);

            //aTimer.Enabled = true;
            //aTimer.AutoReset = true;
            //aTimer.Start();

        }
        public async Task closeConn()
        {
            await client.CloseAsync();
        }
    }
}
