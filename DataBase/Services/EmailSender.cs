﻿
using Microsoft.AspNetCore.Identity;
using StockExchange.DataBase;
using SendGrid;
using SendGrid.Helpers.Mail;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using PdfSharp.Drawing;
using PdfSharp.Pdf;
using PdfSharp.Drawing.Layout;

namespace StockExchange.MiddleWare
{
    public interface IEmailSender
    {
        public Task Execute(string email);
        public string SendEmail(string to, string from, string subject, int idRezervare);
    }
    public class EmailSender : IEmailSender
    {
        private readonly DatabaseContext db;
        private readonly UserManager<StockExchange.DataBase.Entities.User> userManager;
        public EmailSender(DatabaseContext databaseContext, UserManager<StockExchange.DataBase.Entities.User> userManager)
        {
            this.db = databaseContext;
            this.userManager = userManager;
        }

        public async Task Execute(string email)
        {
            var user = db.User.FirstOrDefault(x => x.Email == email);
            if (user == null) return;
            var apiKey = Environment.GetEnvironmentVariable("SENDGRID_API_KEY", EnvironmentVariableTarget.User);
            //string apiKey = "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";
            //https://app.sendgrid.com/email_activity
            var client = new SendGridClient(apiKey);
            var from = new EmailAddress("marcupalii@gmail.com", "Stock Exchange");
            var subject = "Password Reset";

            var to = new EmailAddress(email, $"{user.FirstName} {user.LastName}");
            var urlReset = "http://localhost:56620/Account/RetypePassword?token1=" + HttpUtility.UrlEncode(user.Id) + "&token2=" + HttpUtility.UrlEncode(user.PasswordHash);
            var html = $"<div>Reset the password for user {user.FirstName} {user.LastName} :" +
                $"<a href=\"{urlReset}\" role=\"button\" style=\"color: #fff;background-color: #337ab7;border-color: #2e6da4;display: inline-block;margin-bottom: 0;font-weight: 400;text-align: center;white-space: nowrap;vertical-align: middle;cursor: pointer;border: 1px solid transparent;padding: 6px 12px;font-size: 14px;line-height: 1.42857143;border-radius: 4px;user-select: none;\">Change Password</a>" +
            $"</div>";

            var msg = MailHelper.CreateSingleEmail(from, to, subject, urlReset, html);
            var response = await client.SendEmailAsync(msg);
        }

        public string SendEmail(string to, string from, string subject, int idRezervare)
        {
            try
            {
                var apiKey = Environment.GetEnvironmentVariable("SENDGRID_API_KEY", EnvironmentVariableTarget.User);

     
                var user = db.User.FirstOrDefault();


                string text = $"Nume si prenume beneficiar: {user.FirstName} {user.LastName} \n" +
                    $"Exchange : test\n";


                PdfDocument document = new PdfDocument();

                PdfPage page = document.AddPage();
                XGraphics gfx = XGraphics.FromPdfPage(page);
                XFont font = new XFont("Times New Roman", 10, XFontStyle.Bold);
                XTextFormatter tf = new XTextFormatter(gfx);


                var rectTitle = new XRect(0, 100, page.Width, 100);
                gfx.DrawRectangle(XBrushes.White, rectTitle);
                tf.Alignment = XParagraphAlignment.Center;
                tf.DrawString("Chitanta inchiriere masina", font, XBrushes.Black, rectTitle, XStringFormats.TopLeft);

                XRect rect = new XRect(40, 200, page.Width, page.Height - 100);
                gfx.DrawRectangle(XBrushes.White, rect);
                tf.Alignment = XParagraphAlignment.Left;
                tf.DrawString(text, font, XBrushes.Black, rect, XStringFormats.TopLeft);

                const string filename = "chitanta.pdf";
                var sourcePath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\", filename);

                document.Save(sourcePath);


                var client = new SendGridClient(apiKey);
                var sender = new EmailAddress(from);
                var receiver = new EmailAddress(to);
                var body = "Informatiile referitoare la plata masinii inchiriate sunt atajate in fisierul de mai jos";
                var msg = MailHelper.CreateSingleEmail(sender, receiver, subject, body, "");
                var bytes = File.ReadAllBytes(sourcePath);
                var file = Convert.ToBase64String(bytes);
                msg.AddAttachment("chitanta.pdf", file);
                var response = client.SendEmailAsync(msg).Result;

                File.Delete(sourcePath);
                return response.ToString();
            }
            catch (Exception exception)
            {
                return exception.ToString();
            }

        }
    }
}
