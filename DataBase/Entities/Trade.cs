﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using static Extensions.Extensions;

namespace StockExchange.DataBase.Entities
{
    public class Trade
    {
        [Key]
        [Required]
        public Guid Id { get; set; }
        [Required]
        public virtual User User { get; set; }
        [Required]
        public virtual Stock Stock { get; set; }
        public float Amount { get; set; }
        public float Price { get; set; }
        public OperationType OperationType { get; set; }
        public DateTime Date { get; set; }
        public Trade()
        {
            this.Id = Guid.NewGuid();
            this.Date = DateTime.Now;
        }
    }
}
