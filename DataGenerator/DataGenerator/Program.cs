﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Azure.ServiceBus;
using Microsoft.Extensions.Configuration;

using StockExchange.DataGenerator.Streams;

class Program
{

    public static async Task Main(string[] args)
    {
        //var userStream = new NewUserStream();

        //var userThread = new Thread(new ThreadStart(userStream.generateData));
        //userThread.IsBackground = true;
        //List<Thread> threads = new List<Thread>() {
        //   userThread
        //};

        var userStream = new NewUserStream();
        var stocksVariantion = new StocksVariationStream();
        var tradeStream = new TradeStream();
        List<Thread> threads = new List<Thread>() {
            new Thread(new ThreadStart(userStream.generateData)),
            new Thread(new ThreadStart(stocksVariantion.generateData)),
            new Thread(new ThreadStart(tradeStream.generateData))
        };

        threads.ForEach(x => x.Start());


        Console.WriteLine("Press \'q\' to quit the sample.");
        string input = Console.ReadLine();
        while (input != "q")
        {
            input = Console.ReadLine();
        }

        threads.ForEach(x => x.Join());
    }
}