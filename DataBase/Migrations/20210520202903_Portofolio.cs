﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace StockExchange.DataBase.Migrations
{
    public partial class Portofolio : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Portofolio",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    UserId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Portofolio", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Trade",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    UserId = table.Column<string>(nullable: false),
                    StockId = table.Column<Guid>(nullable: false),
                    Amount = table.Column<float>(nullable: false),
                    Price = table.Column<float>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Trade", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Trade_Stock_StockId",
                        column: x => x.StockId,
                        principalTable: "Stock",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Trade_User_UserId",
                        column: x => x.UserId,
                        principalTable: "User",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PortofolioItems",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    QuantityOwn = table.Column<float>(nullable: false),
                    Money = table.Column<float>(nullable: false),
                    StockId = table.Column<Guid>(nullable: false),
                    PortofolioId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PortofolioItems", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PortofolioItems_Portofolio_PortofolioId",
                        column: x => x.PortofolioId,
                        principalTable: "Portofolio",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PortofolioItems_Stock_StockId",
                        column: x => x.StockId,
                        principalTable: "Stock",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_PortofolioItems_PortofolioId",
                table: "PortofolioItems",
                column: "PortofolioId");

            migrationBuilder.CreateIndex(
                name: "IX_PortofolioItems_StockId",
                table: "PortofolioItems",
                column: "StockId");

            migrationBuilder.CreateIndex(
                name: "IX_Trade_StockId",
                table: "Trade",
                column: "StockId");

            migrationBuilder.CreateIndex(
                name: "IX_Trade_UserId",
                table: "Trade",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PortofolioItems");

            migrationBuilder.DropTable(
                name: "Trade");

            migrationBuilder.DropTable(
                name: "Portofolio");
        }
    }
}
