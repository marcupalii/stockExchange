﻿using AzureServices;
using Microsoft.AspNetCore.Mvc;
using StockExchange.MiddleWare;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StockExchange.Web.Controllers
{
    public class AdminDashboardController : Controller
    {
        private readonly IUser userService;
        public AdminDashboardController(IUser userMiddleWare)
        {

        }
        public IActionResult Index()
        {
            return View();
        }


        [Route("AdminDashboardController/Tradelasthour")]
        [HttpGet]
        public IActionResult GetChartData()
        {
            var res = new StorageBlobService().Tradelasthour();

            return Json(res);
        }

        [Route("/AdminDashboardController/UsersPerPeriodChart")]
        [HttpGet]
        public IActionResult UsersPerPeriodChart()
        {
            var res = new StorageBlobService().Usersperperiod();

            return Json(res);
        }

        [Route("/AdminDashboardController/ActiveUsers")]
        [HttpGet]
        public IActionResult ActiveUsers()
        {
            var res = new StorageBlobService().ActiveUsers();

            return Json(res);
        }
        
    }
}
