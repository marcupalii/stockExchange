﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StockExchange.DataBase.Models
{
    public class IdDisplayItem
    {
        public Guid Id { get; set; }
        public string Display { get; set; }
    }
}
