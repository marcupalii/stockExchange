﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace StockExchange.DataBase.Entities
{
    public class PortofolioItems
    {
        [Key]
        [Required]
        public Guid Id { get; set; }
        [Required]
        public float QuantityOwn { get; set; }
        [Required]
        public float Money { get; set; }
        [Required]
        public virtual Stock Stock { get; set; }
        public Guid PortofolioId  { get; set; }
        public PortofolioItems()
        {
            this.Id = Guid.NewGuid();
            QuantityOwn = 0;
            Money = 0;
        }
    }
}
