﻿
$(document).ready(function () {
    "use strict";


    var Tradelasthour = new Chart(document.getElementById("Tradelasthour").getContext("2d"), {
        type: 'line',
        data: {
            labels: [],
            datasets: [{
                label: '#Last Hour',
                data: [0, 0, 0, 0, 0, 0, 0],
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)'
                    //'rgba(54, 162, 235, 0.2)',
                    //'rgba(255, 206, 86, 0.2)',
                    //'rgba(75, 192, 192, 0.2)',
                    //'rgba(153, 102, 255, 0.2)',
                    //'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)'
                    //    'rgba(54, 162, 235, 1)',
                    //    'rgba(255, 206, 86, 1)',
                    //    'rgba(75, 192, 192, 1)',
                    //    'rgba(153, 102, 255, 1)',
                    //    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            responsive: true,
            plugins: {
                legend: {
                    position: 'top',
                },
                title: {
                    display: true,
                    text: 'Trades in the last hour'
                }
            },
            scales: {
                x: {
                    display: true,
                    title: {
                        display: true,
                        text: 'Minute'
                    }
                },
                y: {
                    display: true,
                    title: {
                        display: true,
                        text: 'Count Trades'
                    }
                }
            }
        },
    });

    var UsersPerPeriodChart = new Chart(document.getElementById("UsersPerPeriodChart").getContext("2d"), {
        type: 'line',
        data: {
            labels: [],
            datasets: [{
                label: '#Last Hour',
                data: [0, 0, 0, 0, 0, 0, 0],
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)'
                    //'rgba(54, 162, 235, 0.2)',
                    //'rgba(255, 206, 86, 0.2)',
                    //'rgba(75, 192, 192, 0.2)',
                    //'rgba(153, 102, 255, 0.2)',
                    //'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)'
                    //    'rgba(54, 162, 235, 1)',
                    //    'rgba(255, 206, 86, 1)',
                    //    'rgba(75, 192, 192, 1)',
                    //    'rgba(153, 102, 255, 1)',
                    //    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            responsive: true,
            plugins: {
                legend: {
                    position: 'top',
                },
                title: {
                    display: true,
                    text: 'User register in the last hour'
                }
            },
            scales: {
                x: {
                    display: true,
                    title: {
                        display: true,
                        text: 'Minute'
                    }
                },
                y: {
                    display: true,
                    title: {
                        display: true,
                        text: 'Count Users'
                    }
                }
            }
        },
    })
    var ActiveUsers = new Chart(document.getElementById("ActiveUsers").getContext("2d"), {
        type: 'line',
        data: {
            labels: [],
            datasets: [{
                label: '#Last Hour',
                data: [0, 0, 0, 0, 0, 0, 0],
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)'
                    //'rgba(54, 162, 235, 0.2)',
                    //'rgba(255, 206, 86, 0.2)',
                    //'rgba(75, 192, 192, 0.2)',
                    //'rgba(153, 102, 255, 0.2)',
                    //'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)'
                    //    'rgba(54, 162, 235, 1)',
                    //    'rgba(255, 206, 86, 1)',
                    //    'rgba(75, 192, 192, 1)',
                    //    'rgba(153, 102, 255, 1)',
                    //    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            responsive: true,
            plugins: {
                legend: {
                    position: 'top',
                },
                title: {
                    display: true,
                    text: 'Newly users that traded within first minute after register'
                }
            },
            scales: {
                x: {
                    display: true,
                    title: {
                        display: true,
                        text: 'Minute'
                    }
                },
                y: {
                    display: true,
                    title: {
                        display: true,
                        text: 'Count Users'
                    }
                }
            }
        },
    })


    $.ajax({
        type: 'get',
        dataType: 'json',
        cache: false,
        url: `/AdminDashboardController/UsersPerPeriodChart`,

        success: function (response, textStatus, jqXHR) {
            UsersPerPeriodChart.data.labels = response.Labels;
            UsersPerPeriodChart.data.datasets[0].data = response.ActiveUsers;
            UsersPerPeriodChart.update();

        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert('Error - ' + errorThrown);
        }
    });

    $.ajax({
        type: 'get',
        dataType: 'json',
        cache: false,
        url: `/AdminDashboardController/Tradelasthour`,

        success: function (response, textStatus, jqXHR) {
            Tradelasthour.data.labels = response.Labels;
            Tradelasthour.data.datasets[0].data = response.Tradelasthour;
            Tradelasthour.update();

        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert('Error - ' + errorThrown);
        }
    });


    $.ajax({
        type: 'get',
        dataType: 'json',
        cache: false,
        url: `/AdminDashboardController/ActiveUsers`,

        success: function (response, textStatus, jqXHR) {
            ActiveUsers.data.labels = response.Labels;
            ActiveUsers.data.datasets[0].data = response.Tradelasthour;
            ActiveUsers.update();

        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert('Error - ' + errorThrown);
        }
    });


});

