﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using AzureServices;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace SampleHost
{
    public static class Program
    {
        public static IConfiguration Configuration;
        public static async Task Main(string[] args)
        {
            //try {
                ;

                //var blobService = new StorageBlobService();
                //await blobService.readTrades();
                //}
                //catch (Exception ex)
                //{
                //    var t = ex;
                //}

            var builder = new HostBuilder()
            .ConfigureAppConfiguration((context, config) =>
            {

                config.AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);
                var builtConfig = config.Build();
                Dictionary<string, string> azureWebJobsSecrets = new Dictionary<string, string>();
                azureWebJobsSecrets["AzureWebJobsStorage"] = builtConfig["ConnectionStrings:StgAccConnection"];
                azureWebJobsSecrets["ConnectionString"] = builtConfig["ConnectionStrings:HubConnection"];
                config.AddInMemoryCollection(azureWebJobsSecrets);
                Program.Configuration = config.Build();
            })
            .ConfigureLogging((context, logging) =>
            {
                logging.AddConsole();
            })
            .ConfigureWebJobs((context) =>
            {
                // context.ad
                context.AddTimers();
                context.AddAzureStorage();
                context.AddAzureStorageCoreServices();
                context.AddEventHubs();

            }).UseConsoleLifetime();

            //.ConfigureServices((services) =>
            //{
            //    services.AddSignalR();
            //})

            var host = builder.Build();
            // https://stackoverflow.com/questions/58412863/illegal-connection-string
            using (host)
            {
                host.RunAsync().Wait();
            }
        } 
    }
}
