﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StockExchange.DataBase.Models
{
    public class UsersPerPeriodChart
    {
        public List<string> Labels { get; set; }
        public List<int> ActiveUsers { get; set; }
        public UsersPerPeriodChart()
        {
            this.Labels = new List<string>();
            this.ActiveUsers = new List<int>();
        }
    }
    public class UsersPerPeriod
    {
        public int Count { get; set; }
        public DateTime Time { get; set; }
    }
}
