﻿
using Azure.Storage.Blobs;
using Azure.Storage.Blobs.Models;
using Microsoft.Azure.Storage;
using Microsoft.Azure.Storage.Blob;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using StockExchange.DataBase;
using StockExchange.DataBase.Entities;
using StockExchange.DataBase.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using static Extensions.Extensions;

namespace AzureServices
{

    public class StorageBlobService 
    {

        public StorageBlobService(){ }
      
        public TradelasthourChart Tradelasthour()
        {
            var TradelasthourChart = new TradelasthourChart();

            IConfiguration config = new ConfigurationBuilder()
               .AddJsonFile("appsettings.json", true, true)
               .Build();
            var connectionString = config["ConnectionStrings:StgAccConnection"];
            var containerName = config["ConnectionStrings:ContainerTradelasthour"];
            var stgAccName = config["ConnectionStrings:StgAccName"];

            BlobContainerClient blobContainerClient = new BlobContainerClient(connectionString, containerName);
            blobContainerClient.CreateIfNotExists();

            var blobs = blobContainerClient.GetBlobs();
            var latestBlob = blobs
                .OfType<BlobItem>()
                .OrderByDescending(m => m.Properties.LastModified)
                .First();


            var blobClient = blobContainerClient.GetBlobClient(latestBlob.Name);

            MemoryStream blobContent = new MemoryStream();
            blobClient.DownloadTo(blobContent);


            var sr = new StreamReader(blobContent, Encoding.UTF8);

            sr.BaseStream.Seek(0, SeekOrigin.Begin);
            var content = sr.ReadToEnd();
            var content_ = content.Split("\n").ToList();
            List<Tradelasthour> usersPeriod = content_.Select(x => JsonConvert.DeserializeObject<Tradelasthour>(x)).OrderByDescending(x => x.Time).ToList();



            var end = DateTime.Now.AddHours(-3); // diff between local and sv time
            var start = end.AddHours(-1);
            var minutes = new List<int>();
            while (start != end)
            {
                minutes.Add(start.Minute);
                start = start.AddMinutes(1);
            }

            TradelasthourChart.Labels = minutes.Select(x => x.ToString()).ToList();
            foreach (var min in minutes)
            {

                var item = usersPeriod.FirstOrDefault(x =>
                    x.Time.Date.Date == start.Date.Date &&
                    x.Time.Hour >= end.AddHours(-1).Hour && x.Time.Hour <= end.Hour &&
                     x.Time.Minute == min
                );
                var count = item != null ? item?.Count : 0;
                TradelasthourChart.Tradelasthour.Add(count.Value);
            }

            return TradelasthourChart;
        }
        
        public ActiveUsersChart ActiveUsers()
        {
            var ActiveUsersChart = new ActiveUsersChart();

            IConfiguration config = new ConfigurationBuilder()
               .AddJsonFile("appsettings.json", true, true)
               .Build();
            var connectionString = config["ConnectionStrings:StgAccConnection"];
            var containerName = config["ConnectionStrings:ContainerActiveUsers"];
            var stgAccName = config["ConnectionStrings:StgAccName"];

            BlobContainerClient blobContainerClient = new BlobContainerClient(connectionString, containerName);
            blobContainerClient.CreateIfNotExists();

            var blobs = blobContainerClient.GetBlobs();
            var latestBlob = blobs
                .OfType<BlobItem>()
                .OrderByDescending(m => m.Properties.LastModified)
                .FirstOrDefault();

            var end = DateTime.Now.AddHours(-3); // diff between local and sv time
            var start = end.AddHours(-1);
            var minutes = new List<int>();
            while (start != end)
            {
                minutes.Add(start.Minute);
                start = start.AddMinutes(1);
            }

            ActiveUsersChart.Labels = minutes.Select(x => x.ToString()).ToList();


            if (latestBlob == null)
            {
                ActiveUsersChart.ActiveUsers = new List<int>(ActiveUsersChart.Labels.Count);
                return ActiveUsersChart;
            }
            var blobClient = blobContainerClient.GetBlobClient(latestBlob.Name);

            MemoryStream blobContent = new MemoryStream();
            blobClient.DownloadTo(blobContent);


            var sr = new StreamReader(blobContent, Encoding.UTF8);

            sr.BaseStream.Seek(0, SeekOrigin.Begin);
            var content = sr.ReadToEnd();
            var content_ = content.Split("\n").ToList();
            List<ActiveUsers> usersPeriod = content_.Select(x => JsonConvert.DeserializeObject<ActiveUsers>(x)).OrderByDescending(x => x.Time).ToList();



  
            foreach (var min in minutes)
            {

                var item = usersPeriod.FirstOrDefault(x =>
                    x.Time.Date.Date == start.Date.Date &&
                    x.Time.Hour >= end.AddHours(-1).Hour && x.Time.Hour <= end.Hour &&
                     x.Time.Minute == min
                );
                var count = item != null ? item?.Count : 0;
                ActiveUsersChart.ActiveUsers.Add(count.Value);
            }

            return ActiveUsersChart;
        }
        public UsersPerPeriodChart Usersperperiod()
        {
            var UsersPerPeriodChart = new UsersPerPeriodChart();

            IConfiguration config = new ConfigurationBuilder()
               .AddJsonFile("appsettings.json", true, true)
               .Build();
            var connectionString = config["ConnectionStrings:StgAccConnection"];
            var containerName = config["ConnectionStrings:ContainerUserPerPeriod"];
            var stgAccName = config["ConnectionStrings:StgAccName"];

            BlobContainerClient blobContainerClient = new BlobContainerClient(connectionString, containerName);
            blobContainerClient.CreateIfNotExists();

            var blobs = blobContainerClient.GetBlobs();
            var latestBlob = blobs
                .OfType<BlobItem>()
                .OrderByDescending(m => m.Properties.LastModified)
                .First();

         
            var blobClient = blobContainerClient.GetBlobClient(latestBlob.Name);

            MemoryStream blobContent = new MemoryStream();
            blobClient.DownloadTo(blobContent);


            var sr = new StreamReader(blobContent, Encoding.UTF8);

            sr.BaseStream.Seek(0, SeekOrigin.Begin);
            var content = sr.ReadToEnd();
            var content_ = content.Split("\n").ToList();
            List<UsersPerPeriod> usersPeriod = content_.Select(x => JsonConvert.DeserializeObject<UsersPerPeriod>(x)).OrderByDescending(x=>x.Time).ToList();



            var end = DateTime.Now.AddHours(-3); // diff between local and sv time
            var start = end.AddHours(-1);
            var minutes = new List<int>();
            while (start != end)
            {
                minutes.Add(start.Minute);
                start = start.AddMinutes(1);
            }

            UsersPerPeriodChart.Labels = minutes.Select(x => x.ToString()).ToList();
            foreach (var min in minutes)
            {
                
                var item = usersPeriod.FirstOrDefault(x =>
                    x.Time.Date.Date == start.Date.Date &&
                    x.Time.Hour >= end.AddHours(-1).Hour && x.Time.Hour <= end.Hour &&
                     x.Time.Minute == min
                );
                var count = item != null ? item?.Count : 0;
                UsersPerPeriodChart.ActiveUsers.Add(count.Value);
            }

            return UsersPerPeriodChart;
        }
        public static void SaveTradeProcess(List<TradeProcess> trades)
        {
           foreach(var trade_ in trades)
           {
                using(var db = DatabaseContext.getDbInstance())
                {
                    var trade = db.Trade.FirstOrDefault(x => x.Id == trade_.Id);
                    if(trade != null)
                    {
                        continue;
                    }
                    else
                    {
                        using (var transaction = db.Database.BeginTransaction())
                        {
                            try
                            {
                                var user = db.User.FirstOrDefault(x => x.Id == trade_.UserId.ToString());
                                var stock = db.Stock.FirstOrDefault(x => x.Id == trade_.StockId);

                                trade = new StockExchange.DataBase.Entities.Trade()
                                {
                                    Id = trade_.Id,
                                    User = user,
                                    Stock = stock,
                                    Amount = trade_.Quantity,
                                    Price = trade_.Price,
                                    OperationType = (OperationType)trade_.Type,
                                    Date = trade_.Date
                                };
                                db.Trade.Add(trade);
                                db.SaveChanges();
                                transaction.Commit();
                            }
                            catch (Exception ex)
                            {
                                transaction.Rollback();
                            }
                        }
                    }
                 
                }
           }
        }
        public async Task readTrades()
        {
            IConfiguration config = new ConfigurationBuilder()
               .AddJsonFile("appsettings.json", true, true)
               .Build();

            var connectionString = config["ConnectionStrings:StgAccConnection"];
            var containerName = config["ConnectionStrings:ContainerTrade"];
            var stgAccName = config["ConnectionStrings:StgAccName"];

            BlobContainerClient blobContainerClient = new BlobContainerClient(connectionString, containerName);
            blobContainerClient.CreateIfNotExists();  
       
            var blobs = blobContainerClient.GetBlobs();
            var latestBlob = blobs
                .OfType<BlobItem>()
                .OrderByDescending(m => m.Properties.LastModified)
                .ToList()
                .Take(2);

            foreach(var blob in latestBlob)
            {
                var blobClient = blobContainerClient.GetBlobClient(blob.Name);

                MemoryStream blobContent = new MemoryStream();
                blobClient.DownloadTo(blobContent);


                var sr = new StreamReader(blobContent, Encoding.UTF8);

                sr.BaseStream.Seek(0,SeekOrigin.Begin);
                var content = sr.ReadToEnd();
                var content_ = content.Split("\n").ToList();
                List<TradeProcess> trades = content_.Select(x => JsonConvert.DeserializeObject<TradeProcess>(x)).ToList();
                SaveTradeProcess(trades);
            }
        }
    }
}
