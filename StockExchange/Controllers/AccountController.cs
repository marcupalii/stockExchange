﻿using System;
using System.Linq;
using System.Threading.Tasks;
using StockExchange.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using StockExchange.MiddleWare;

namespace StockExchange.Controllers
{
    public class AccountController : Controller
    {
        private Microsoft.Extensions.Logging.ILogger logger;
        private readonly UserManager<StockExchange.DataBase.Entities.User> userManager;
        private readonly SignInManager<StockExchange.DataBase.Entities.User> signInManager;
        private readonly IUser userMiddleWare;
        private readonly IEmailSender emailSender;
        private readonly DataBase.DatabaseContext db;
        public AccountController(ILogger<AccountController> logger, UserManager<DataBase.Entities.User> userManager, SignInManager<DataBase.Entities.User> signInManager, IUser userMiddleWare, IEmailSender emailSender, DataBase.DatabaseContext db)
        {
            this.logger = logger;
            this.userManager = userManager;
            this.signInManager = signInManager;
            this.userMiddleWare = userMiddleWare;
            this.emailSender = emailSender;
            this.db = db;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Logout()
        {

            var user = HttpContext.User;
            await signInManager.SignOutAsync();


            logger.LogInformation("User logged out.");
            return RedirectToAction(nameof(HomeController.Index), "Home");
        }

        [HttpGet]
        public IActionResult Login()
        {
            return View("Login");
        }

        [HttpGet]
        [Authorize]
        public IActionResult Profile()
        {
            var currentUser = userManager.GetUserAsync(HttpContext.User).Result;
            var roles = userManager.GetRolesAsync(currentUser).Result;

            var profile = new Models.Profile()
            {
                LastName = currentUser.LastName,
                FirstName = currentUser.FirstName,
                Address = currentUser.Address,
                Email = currentUser.Email,
                Roles = string.Join(", ", roles)
            };
            return View(profile);
        }
        [HttpGet]
        public IActionResult Register()
        {
            return View("Register");
        }
        [HttpPost]
        public async Task<ActionResult> Register(NewUser user)
        {
            if (ModelState.IsValid)
            {
                var newUser = new StockExchange.DataBase.Entities.User()
                {
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    Email = user.Email,
                    Address = user.Address,
                    UserName = user.Email
                };

                var result = await userManager.CreateAsync(newUser, user.Password);
                if (result.Succeeded)
                {
                    await signInManager.SignInAsync(newUser, true);
                    return RedirectToAction("Profile");
                }
            }

            return View("Register");

        }


        [HttpPost]
        public async Task<IActionResult> Login(LoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                var result = await signInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, false);

                if (result.Succeeded)
                {
                    return RedirectToAction("Profile");
                }

                ModelState.AddModelError(string.Empty, "Invalid Login Attempt");
            }

            return View("Login", model);
        }
        [HttpGet]
        public async Task<IActionResult> ResetPassword()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> ResetPassword(ResetPassword resetPassword)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    emailSender.Execute(resetPassword.Email).Wait();
                    return RedirectToAction("Login");
                }
                catch (Exception exception)
                {

                }
            }
            return View(resetPassword);
        }

        public IActionResult RetypePassword(string token1, string token2)
        {
            var user = userMiddleWare.GetByTokens(token1, token2);
            if (user != null)
            {
                var model = new RetypePassword()
                {
                    Id = user.Id,
                    Email = user.Email
                };
                return View(model);
            }
            else
                return RedirectToAction("Login");
        }
        [HttpPost]
        public async Task<IActionResult> RetypePassword(RetypePassword retypePassword)
        {
            if (ModelState.IsValid)
            {
                userMiddleWare.UpdatePassword(retypePassword);
                return RedirectToAction("Login");
            }
            else
                return View(retypePassword);
        }
    }
}