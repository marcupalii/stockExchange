﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StockExchange.DataBase.Models
{

    public class TradelasthourChart
    {
        public List<string> Labels { get; set; }
        public List<int> Tradelasthour { get; set; }
        public TradelasthourChart()
        {
            this.Labels = new List<string>();
            this.Tradelasthour = new List<int>();
        }
    }
    public class Tradelasthour
    {
        public int Count { get; set; }
        public DateTime Time { get; set; }
    }
}
