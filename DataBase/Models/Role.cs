﻿
using System.ComponentModel.DataAnnotations;

namespace StockExchange.Models
{
    public class Role
    {
        [Required]
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
