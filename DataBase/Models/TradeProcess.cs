﻿using System;
using System.Collections.Generic;
using System.Text;
using static Extensions.Extensions;

namespace StockExchange.DataBase.Models
{
    public class TradeProcess
    {
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public Guid StockId { get; set; }
        public float Price { get; set; }
        public float Quantity { get; set; }
        public DateTime Date { get; set; }
        public OperationType Type { get; set; }
    }
}
