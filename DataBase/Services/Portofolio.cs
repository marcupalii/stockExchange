﻿
using StockExchange.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using StockExchange.DataBase;
using StockExchange.DataBase.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using StockExchange.DataBase.Models;

namespace StockExchange.MiddleWare
{
    public interface IPortofolio
    {
        public void AddPortofolio(StockExchange.DataBase.Models.NewPortofolio model);
        public List<StockExchange.DataBase.Entities.Portofolio> getUserPortofoliosWithStock(Guid userId);
        public StockExchange.DataBase.Entities.Portofolio GetByIdWithStock(Guid? Id);
        public void AddStock(AddToPortofolio stock);

    }
    public class Portofolio : IPortofolio
    {
        private readonly DatabaseContext db;
        private readonly IUser userService;
        public Portofolio(IUser userService,DatabaseContext databaseContext = null)
        {
            this.userService = userService;
            if (databaseContext != null)
            {
                this.db = databaseContext;
            }
            else
            {
                IConfiguration config = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", true, true)
                .Build();

                var connectionString = config["ConnectionStrings:DbConnection"];


                var builder = new DbContextOptionsBuilder();
                builder.UseSqlServer(connectionString)
                       .UseLazyLoadingProxies(true);

                this.db = new DatabaseContext(builder.Options);
            }
        }

        public void AddPortofolio(StockExchange.DataBase.Models.NewPortofolio model)
        {
            using (var db = DatabaseContext.getDbInstance())
            {
                var portofolio = new StockExchange.DataBase.Entities.Portofolio()
                {
                    Id = model.Id,
                    UserId = model.UserId,
                    Name = model.Name
                };

                var entity = db.Portofolio.Add(portofolio);
                db.SaveChanges();
            }
        }
        public List<StockExchange.DataBase.Entities.Portofolio> getUserPortofoliosWithStock(Guid userId)
        {
            using (var db = DatabaseContext.getDbInstance())
            {
                return db.Portofolio.Include("PortofolioItems").Where(x=>x.UserId == userId).ToList();
            }
        }
        public StockExchange.DataBase.Entities.Portofolio GetByIdWithStock(Guid? Id)
        {
            if (Id != null)
            {
                return db.Portofolio.Include("PortofolioItems").FirstOrDefault(x => x.Id == Id);
            }
            else
            {
                return db.Portofolio.Include("PortofolioItems").FirstOrDefault();
            }
           
        }
        public void AddStock(AddToPortofolio model)
        {
            using (var db = DatabaseContext.getDbInstance())
            {
                var portofolio = db.Portofolio.Include("PortofolioItems").FirstOrDefault(x => x.Id == Guid.Parse(model.Id));

                if(portofolio.PortofolioItems.Any(x=>x.Stock.Id == Guid.Parse(model.StockId)))
                {
                    return;
                }
                var stock = db.Stock.FirstOrDefault(x => Guid.Parse(model.StockId) == x.Id);

                var portofolioItem = new PortofolioItems()
                {
                    Stock = stock,
                    PortofolioId = portofolio.Id
                };
               
                db.PortofolioItems.Add(portofolioItem);
                db.SaveChanges();
            }
        }
    }
}
