﻿
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using StockExchange.DataBase.Models;
using StockExchange.MiddleWare;
using System;
using System.Linq;

namespace StockExchange.Web.Controllers
{
    [Authorize]
    public class StockController : Controller
    {
        private ILogger logger;
        private readonly UserManager<StockExchange.DataBase.Entities.User> userManager;

        //private readonly IUser userMiddleWare;
        private readonly IEmailSender emailSender;
        private readonly IStock stockService;
        private readonly IPortofolio portofolioService;
        private readonly StockExchange.DataBase.DatabaseContext db;
        public StockController(ILogger<StockController> logger, IStock stockService, IPortofolio Portofolio, UserManager<StockExchange.DataBase.Entities.User> userManager, IEmailSender emailSender)
        {
            this.logger = logger;
            this.userManager = userManager;
            this.emailSender = emailSender;
            this.stockService = stockService;
   
            this.portofolioService = Portofolio;
        }

        [HttpGet]
  
        public IActionResult Stocks()
        {
            //ViewBag.IdPortofolio = idPortofolio;
            //ViewBag.DataPoints = JsonConvert.SerializeObject(DataService.GetRandomDataForNumericAxis(20));
            return View("GetAll");
        }

        [HttpPost]
        [Authorize]
        public IActionResult GetStocks()
        {
            //var currentUser = userManager.GetUserAsync(HttpContext.User).Result;
            //var roles = userManager.GetRolesAsync(currentUser).Result;
            //if (!roles.Contains("admin"))
            //{
            //    return RedirectToAction("Login", "Account");
            //}
            var length = Convert.ToInt32(Request.Form["length"]);
            var start = Convert.ToInt32(Request.Form["start"]);
            var col = Request.Form[$"columns[{Request.Form["order[0][column]"]}][name]"];
            var search = Request.Form["search[value]"];
            var dir = Request.Form["order[0][dir]"];
            var users = stockService.GetAll(col, dir, search).ToList();
            return Json(new
            {
                data = users.Skip(start).Take(length),
                recordsTotal = users.Count(),
                recordsFiltered = users.Count()
            });
        }

        [HttpGet]
       // [Route("Stock/AddToPortofolio/{id?}")]
        public IActionResult AddToPortofolio(string id)
        {
            var model = new AddToPortofolio()
            {
                StockId = id,
                Id = ""
            };
            try
            {
                var currentUser = userManager.GetUserAsync(HttpContext.User).Result;
                var portofolios = this.portofolioService.getUserPortofoliosWithStock(Guid.Parse(currentUser.Id));
                ViewBag.IdDisplayItems = portofolios.Select(x => new { Id = x.Id, Display = x.Name }).ToList();
              
                return PartialView("_AddToPortofolio", model);
            }catch(Exception ex)
            {
                return PartialView("_AddToPortofolio", model);
            }
        }
        [HttpPost]  
        public IActionResult AddToPortofolio(AddToPortofolio stock) 
        {
            try
            {
                if (ModelState.IsValid)
                {
                    portofolioService.AddStock(stock);
                    return RedirectToAction("Stocks","Stock");
                }
                else
                    return PartialView("_AddToPortofolio", stock);
            }catch(Exception ex)
            {
                return RedirectToAction("Stocks", "Stock");
            }
        }
      

    }
}
