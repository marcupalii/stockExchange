﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace StockExchange.DataBase.Entities
{
    public class Portofolio
    {
        [Key]
        [Required]
        public Guid Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public Guid UserId { get; set; }
        [Required]
        public virtual List<PortofolioItems> PortofolioItems { get; set; }

        public Portofolio()
        {
            this.Id = Guid.NewGuid();
        }
    }
}
