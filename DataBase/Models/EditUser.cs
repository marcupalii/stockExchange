﻿using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace StockExchange.Models
{
    public class EditUser
    {
        [Required]
        public string Id { get; set; }
        [Required]
        [Display(Name = "First Name: ")]
        public string FirstName { get; set; }
        [Required]
        [Display(Name = "Last Name: ")]
        public string LastName { get; set; }
        [Required]
        [EmailAddress(ErrorMessage = "Enter a valid email!")]
        [Display(Name = "Email: ")]
        public string Email { get; set; }
        [Required]
        [Display(Name = "Address: ")]
        public string Address { get; set; }
        [Display(Name = "User roles: ")]
        public List<string> RolesId { get; set; }
        public List<IdentityRole> Roles { get; set; }
        public List<Models.Role> AllRoles { get; set; }

    }
}
