﻿
using System.ComponentModel.DataAnnotations;

namespace StockExchange.Models
{
    public class ResetPassword
    {
        [Required(ErrorMessage = "Email Address is required!")]
        [EmailAddress(ErrorMessage = "Enter a valid email!")]
        [Display(Name = "Email Address: ")]
        public string Email { get; set; }
    }
}
