﻿
using Microsoft.AspNetCore.Identity;
using StockExchange.DataBase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StockExchange.MiddleWare
{
    public interface IRole
    {
        public List<IdentityRole> getRoles();
        public List<IdentityRole> GetUserRoles(StockExchange.DataBase.Entities.User utilizator);
        public Models.Role IdentityRoleToRole(IdentityRole identityRole);
    }
    public class Role : IRole
    {
        private readonly DatabaseContext db;
        private RoleManager<IdentityRole> roleManager;
        public Role(DatabaseContext databaseContext, RoleManager<IdentityRole> roleManager)
        {
            this.db = databaseContext;
            this.roleManager = roleManager;
        }
        public List<IdentityRole> getRoles()
        {
            return roleManager.Roles.ToList();
        }
        public List<IdentityRole> GetUserRoles(StockExchange.DataBase.Entities.User utilizator)
        {
            var rolesAssgined = db.UserRoles.Where(x => x.UserId == utilizator.Id).Select(y=>y.RoleId);
            return db.Roles.Where(x => rolesAssgined.Contains(x.Id)).ToList();
        }

        public Models.Role IdentityRoleToRole(IdentityRole identityRole)
        {
            return new Models.Role()
            {
                Id = identityRole.Id,
                Name = identityRole.Name
            };
        }
    }
}
