﻿
using StockExchange.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using StockExchange.DataBase;
using StockExchange.DataBase.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using StockExchange.DataBase.Models;

namespace StockExchange.MiddleWare
{
    public interface IUser
    {
        public string Edit(Models.EditUser user);
        public List<TableUser> GetAll(string col = "FirstName", string dir = "asc", string search = "");
        public EditUser GetById(string id);
        public StockExchange.DataBase.Entities.User GetById(Guid id);
        public void Delete(string id);
        public StockExchange.DataBase.Entities.User GetByTokens(string token1, string token2);
        public void UpdatePassword(RetypePassword retypePassword);
        public EditUser UtilizatorToEditUser(StockExchange.DataBase.Entities.User utilizator);
        public StockExchange.DataBase.Entities.User EditUserToUtilizator(EditUser editUser);
    }
    public class User : IUser
    {
        private readonly DatabaseContext db;
        private readonly IRole roleMiddleware;
        private readonly UserManager<StockExchange.DataBase.Entities.User> userManager;
        private readonly RoleManager<IdentityRole> roleManager;
        public User(DatabaseContext databaseContext, IRole roleMiddleware, UserManager<StockExchange.DataBase.Entities.User> userManager, RoleManager<IdentityRole> roleManager)
        {
            this.db = databaseContext;
            this.roleMiddleware = roleMiddleware;
            this.userManager = userManager;
            this.roleManager = roleManager;
        }
        public List<TableUser> GetAll(string col, string dir = "asc", string search = "")
        {
            col = col == string.Empty ? "FirstName" : col;
            List<TableUser> users = new List<TableUser>();
            if (search != string.Empty)
            {
                foreach (var user in DatabaseContext.getDbInstance().User)
                {
                    if (user.LastName.Contains(search) || user.FirstName.Contains(search) || user.Address.Contains(search) || user.Email.Contains(search) || userManager.GetRolesAsync(user).Result.Contains(search))
                    {
                        users.Add(new TableUser()
                        {
                            Id = user.Id,
                            FirstName = user.FirstName,
                            LastName = user.LastName,
                            Address = user.Address,
                            Email = user.Email,
                            Roles = string.Join(", ", userManager.GetRolesAsync(user).Result.ToArray())
                        });
                    }
                }
            }
            else
            {
                users = DatabaseContext.getDbInstance().User.Select(user => new TableUser()
                {
                    Id = user.Id,
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    Address = user.Address,
                    Email = user.Email,
                    Roles = string.Join(", ", userManager.GetRolesAsync(user).Result.ToArray()),
                    Active = user.Active
                }).ToList();
            }
            if (dir == "asc")
            {
                return users.OrderBy(x => x.GetType().GetProperty(col).GetValue(x)).ToList();
            }
            else
            {
                return users.OrderByDescending(x => x.GetType().GetProperty(col).GetValue(x)).ToList();
            }

        }
        public EditUser GetById(string id)
        {
            return UtilizatorToEditUser(db.User.FirstOrDefault(x => x.Id == id));
        }
        public EditUser UtilizatorToEditUser(StockExchange.DataBase.Entities.User utilizator)
        {
            var allRoles = roleMiddleware.getRoles();
            var roles = roleMiddleware.GetUserRoles(utilizator);
            return new EditUser()
            {
                Id = utilizator.Id,
                LastName = utilizator.LastName,
                FirstName = utilizator.FirstName,
                Email = utilizator.Email,
                Address = utilizator.Address,
                Roles = roleMiddleware.GetUserRoles(utilizator),
                AllRoles = roleMiddleware.getRoles().Select(x => roleMiddleware.IdentityRoleToRole(x)).ToList()
            };
        }
        public StockExchange.DataBase.Entities.User EditUserToUtilizator(EditUser editUser)
        {
            return new StockExchange.DataBase.Entities.User()
            {
                Id = editUser.Id,
                Email = editUser.Email,
            };
        }
        public string Edit(Models.EditUser user)
        {
            using (IDbContextTransaction transaction = db.Database.BeginTransaction())
            {
                try
                {
                    var userEntity = db.User.FirstOrDefault(x => x.Id == user.Id);
                    userEntity.Address = user.Address;
                    userEntity.Email = user.Email;
                    userEntity.FirstName = user.FirstName;
                    userEntity.LastName = user.LastName;

                    foreach (var role in roleMiddleware.GetUserRoles(EditUserToUtilizator(user)))
                    {
                        if (!user.RolesId.Contains(role.Id))
                        {
                            var userRole = db.UserRoles.FirstOrDefault(x => x.UserId == user.Id && x.RoleId == role.Id);
                            db.UserRoles.Remove(userRole);
                        }
                    }

                    db.SaveChanges();
                    db.Entry(userEntity).Reload();

                    foreach (var roleId in user.RolesId)
                    {
                        if (!roleMiddleware.GetUserRoles(userEntity).Select(x => x.Id).Contains(roleId))
                        {
                            var role = db.Roles.FirstOrDefault(x => x.Id == roleId);
                            var roleAsigned = new IdentityUserRole<string>()
                            {
                                UserId = userEntity.Id,
                                RoleId = role.Id
                            };
                            db.UserRoles.Add(roleAsigned);

                        }
                    }
                    db.SaveChanges();
                    transaction.Commit();
                    return user.Id;
                }
                catch
                {
                    transaction.Rollback();
                    return user.Id;
                }
            }

        }
        public void Delete(string id)
        {
            using (IDbContextTransaction transaction = db.Database.BeginTransaction())
            {
                try
                {
                    var user = db.User.FirstOrDefault(x => x.Id == id);
                    user.Active = false;
                    //var asignedRole = db.UserRoles.FirstOrDefault(x => x.UserId == id);
                    //db.UserRoles.Remove(asignedRole);
                    //db.User.Remove(user);
                    db.SaveChanges();
                    transaction.Commit();
                }
                catch
                {
                    transaction.Rollback();
                }
            }

        }
        public StockExchange.DataBase.Entities.User GetByTokens(string token1, string token2)
        {
            return db.User.FirstOrDefault(x => x.Id == token1 && x.PasswordHash == token2);
        }
        public void UpdatePassword(RetypePassword retypePassword)
        {
            using (IDbContextTransaction transaction = db.Database.BeginTransaction())
            {
                try
                {
                    var user = db.User.FirstOrDefault(x => x.Id == retypePassword.Id);
                    user.PasswordHash = userManager.PasswordHasher.HashPassword(user, retypePassword.Password);
                    db.Entry(user).State = EntityState.Modified;
                    db.SaveChanges();
                    transaction.Commit();
                }
                catch
                {
                    transaction.Rollback();
                }
            }
        }
        public StockExchange.DataBase.Entities.User GetById(Guid id)
        {
            using (IDbContextTransaction transaction = db.Database.BeginTransaction())
            {
               return db.User.FirstOrDefault(x => x.Id == id.ToString());
            }
        }
    }
}
