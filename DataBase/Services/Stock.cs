﻿
using StockExchange.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using StockExchange.DataBase;
using StockExchange.DataBase.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using StockExchange.DataBase.Models;
using static Extensions.Extensions;

namespace StockExchange.MiddleWare
{
    public interface IStock
    {
     //   public string Edit(Models.EditUser user);
        public List<StockExchange.DataBase.Entities.Stock> GetAll(string col = "FirstName", string dir = "asc", string search = "");
        public StockExchange.DataBase.Entities.Stock GetById(Guid? id);
        public void Add(TradeModel model, Guid userId);
        public List<StockHistory> getStockHistoryById(List<Guid> stockIds);
    }
    public class Stock : IStock
    {
        private readonly DatabaseContext db;

        public Stock( DatabaseContext databaseContext = null)
        {
            if (databaseContext != null)
            {
                this.db = databaseContext;
            }
            else
            {
                IConfiguration config = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", true, true)
                .Build();

                var connectionString = config["ConnectionStrings:DbConnection"];


                var builder = new DbContextOptionsBuilder();
                builder.UseSqlServer(connectionString)
                       .UseLazyLoadingProxies(true);

                this.db = new DatabaseContext(builder.Options);
            }
        }
        public List<StockExchange.DataBase.Entities.Stock> GetAll(string col, string dir = "asc", string search = "")
        {
            col = col == string.Empty ? "Name" : col;
            var stocks = new List<StockExchange.DataBase.Entities.Stock>();
            if (search != string.Empty)
            {
                foreach (var stock in db.Stock)
                {
                    if (stock.Name.Contains(search) || stock.AvailableQuantity.ToString().Contains(search) || stock.Price.ToString().Contains(search))
                    {
                        stocks.Add(stock);
                    }
                }
            }
            else
            {
                stocks = db.Stock.ToList();
            }
            if (dir == "asc")
            {
                return stocks.OrderBy(x => x.GetType().GetProperty(col).GetValue(x)).ToList();
            }
            else
            {
                return stocks.OrderByDescending(x => x.GetType().GetProperty(col).GetValue(x)).ToList();
            }

        }
        public void Update(StockExchange.DataBase.Entities.Stock stock)
        {
            using (var db = DatabaseContext.getDbInstance())
            {
                var entity = db.Stock.FirstOrDefault(x => x.Id == stock.Id);
                if (entity == null) return;
                entity.Name = stock.Name;
                entity.Price = stock.Price;
                entity.AvailableQuantity = stock.AvailableQuantity;
                entity.RiskIndices = stock.RiskIndices;
                entity.ToTalQuantity = stock.ToTalQuantity;
                db.SaveChanges();
            }
        }
        public StockExchange.DataBase.Entities.Stock GetById(Guid? id)
        {
            using (var db = DatabaseContext.getDbInstance())
            {
                return db.Stock.FirstOrDefault(x => x.Id == id.Value);
            }
        }
        public void Add(TradeModel model, Guid userId)
        {
            using (var db = DatabaseContext.getDbInstance())
            {
                using(var tran = db.Database.BeginTransaction())
                {
                    try
                    {
                        var user = db.User.FirstOrDefault(x => x.Id == userId.ToString());
                        var stock = db.Stock.FirstOrDefault(x => x.Id == model.StockId);

                        var trade = new StockExchange.DataBase.Entities.Trade()
                        {
                            Id = model.Id,
                            User = user,
                            Stock = stock,
                            Amount = model.Quantity,
                            Price = model.PriceStock,
                            OperationType = (OperationType)model.OperationType
                        };
                        db.SaveChanges();


                        var portofolio = db.Portofolio.Include("PortofolioItems").FirstOrDefault(x => x.Id == model.PortofolioId);
                        var portofolioItem = portofolio.PortofolioItems.FirstOrDefault(x => x.Stock.Id == stock.Id);


                        if (model.OperationType == (int)OperationType.Sell)
                        {
                            stock.AvailableQuantity += model.Quantity;   

                            portofolioItem.Money -= model.TotalPrice;
                            portofolioItem.QuantityOwn -= model.Quantity;
                        }
                        else
                        {
                            stock.AvailableQuantity -= model.Quantity;

                            portofolioItem.Money += model.TotalPrice;
                            portofolioItem.QuantityOwn += model.Quantity;
                        }

                  
                        

                        db.Trade.Add(trade);
                        db.SaveChanges();
                        tran.Commit();
                    }
                    catch (Exception ex)
                    {
                        tran.Rollback();
                    }
                }


               
            }
        
            
        }
        public List<StockHistory> getStockHistoryById(List<Guid> stockIds)
        {
            var result = new List<StockHistory>();
            var start = DateTime.Now.AddDays(-6);
            var days = new List<DayOfWeek>();
            while (start.Date.Day != DateTime.Now.AddDays(1).Date.Day) {
                days.Add(start.DayOfWeek);
                start = start.AddDays(1);
            }
          
            using (var db = DatabaseContext.getDbInstance())
            {
                foreach (var id in stockIds)
                {
                    var stockHistory = new StockHistory() { StockId = id };
                    var trades = db.Trade.Where(x => x.Id == id).ToList();
                    var stock = db.Stock.FirstOrDefault(x => x.Id == id);

                    foreach (DayOfWeek day in days)
                    {
                        var history = new History() { Day = day };
                        var trades_ = trades.Where(x => x.Date.DayOfWeek == day);

                        var prices = trades_.Where(x => x.Date.DayOfWeek == day).Select(x => x.Price).ToList();
                        if(day == DateTime.Now.DayOfWeek)
                        {
                            prices.Add(stock.Price);
                        }
                       
                    
                        history.Avg = prices.Count == 0? 0:  prices.Average(x => x);

                        stockHistory.Values.Add(history);
                    }
                  
                    result.Add(stockHistory);
                }
            }
            return result;
        }
    }
}
