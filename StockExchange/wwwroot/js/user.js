﻿$(document).ready(function () {

    $('#modal-title-edit-user').text('Edit user');
    $('#RolesId').select2();
    $('#button-save').on("click", function () {
        $('#form-edit-user').submit();
    });
});