﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace StockExchange.DataBase.Entities
{
    public class Stock
    {
        [Key]
        [Required]
        public Guid Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public float Price { get; set; }
        [Required]
        public float ToTalQuantity { get; set; }
        public float AvailableQuantity { get; set; }
        public float RiskIndices { get; set; }
        public Stock()
        {
            this.Id = Guid.NewGuid();
        }
    }
}
