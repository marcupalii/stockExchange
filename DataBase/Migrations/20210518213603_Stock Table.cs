﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace StockExchange.DataBase.Migrations
{
    public partial class StockTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Stock",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    Price = table.Column<float>(nullable: false),
                    ToTalQuantity = table.Column<float>(nullable: false),
                    AvailableQuantity = table.Column<float>(nullable: false),
                    RiskIndices = table.Column<float>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Stock", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Stock");
        }
    }
}
