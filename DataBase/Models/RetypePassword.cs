﻿
using System.ComponentModel.DataAnnotations;

namespace StockExchange.Models
{
    public class RetypePassword
    {
        [Required]
        public string Id { get; set; }

        [Required]
        [DataType(dataType: DataType.EmailAddress)]
        public string Email { get; set; }

        [Required(ErrorMessage = "Field Password is required!")]
        [DataType(dataType: DataType.Password)]
        [Display(Name = "Password: ")]
        public string Password { get; set; }

        [Required(ErrorMessage = "Field ConfirmPassword is required!")]
        [DataType(dataType: DataType.Password)]
        [Compare("Password", ErrorMessage = "Password and ConfirmPassword do not match!")]
        [Display(Name = "ConfirmPassword: ")]
        public string ConfirmPassword { get; set; }
    }
}
