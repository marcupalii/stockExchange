﻿
using StockExchange.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using StockExchange.MiddleWare;

namespace StockExchange.Controllers
{
    public class UsersController : Controller
    {
        private readonly IUser userMiddleWare;
        private readonly IRole roleMiddleWare;
        private readonly UserManager<StockExchange.DataBase.Entities.User> userManager;

        public UsersController(IUser userMiddleWare, IRole roleMiddleWare, UserManager<StockExchange.DataBase.Entities.User> userManager)
        {
            this.userMiddleWare = userMiddleWare;
            this.roleMiddleWare = roleMiddleWare;
            this.userManager = userManager;
        }
        [HttpGet]
        [Authorize]
        public IActionResult EditUser(string id)
        {
            var user = userManager.GetUserAsync(HttpContext.User).Result;
            var roles = userManager.GetRolesAsync(user).Result;
            if (!roles.Contains("admin"))
            {
                return RedirectToAction("Login", "Account");
            }
            return PartialView("_EditUser", userMiddleWare.GetById(id));
        }
        [HttpPost]
        [Authorize]
        public IActionResult EditUser(EditUser user)
        {
            var currentUser = userManager.GetUserAsync(HttpContext.User).Result;
            var roles = userManager.GetRolesAsync(currentUser).Result;
            if (!roles.Contains("admin"))
            {
                return RedirectToAction("Login", "Account");
            }
            if (ModelState.IsValid)
            {
                var id = userMiddleWare.Edit(user);
                return RedirectToAction("GetAll");
            }
            else
                return PartialView("_EditUser", user);
        }
        [HttpGet]
        [Authorize]
        public IActionResult GetAll()
        {
            var currentUser = userManager.GetUserAsync(HttpContext.User).Result;
            var roles = userManager.GetRolesAsync(currentUser).Result;
            if (!roles.Contains("admin"))
            {
                return RedirectToAction("Login", "Account");
            }
            var users = userMiddleWare.GetAll();
            return View();
        }

        [HttpPost]
        [Authorize]
        public IActionResult GetUsers()
        {
            var currentUser = userManager.GetUserAsync(HttpContext.User).Result;
            var roles = userManager.GetRolesAsync(currentUser).Result;
            if (!roles.Contains("admin"))
            {
                return RedirectToAction("Login", "Account");
            }
            var length = Convert.ToInt32(Request.Form["length"]);
            var start = Convert.ToInt32(Request.Form["start"]);
            var col = Request.Form[$"columns[{Request.Form["order[0][column]"]}][name]"];
            var search = Request.Form["search[value]"];
            var dir = Request.Form["order[0][dir]"];
            var users = userMiddleWare.GetAll(col, dir, search).ToList();
            return Json(new
            {
                data = users.Skip(start).Take(length),
                recordsTotal = users.Count(),
                recordsFiltered = users.Count()
            });
        }
        [Authorize]
        public IActionResult DeleteUser(string id)
        {
            var currentUser = userManager.GetUserAsync(HttpContext.User).Result;
            var roles = userManager.GetRolesAsync(currentUser).Result;
            if (!roles.Contains("admin"))
            {
                return RedirectToAction("Login", "Account");
            }
            try
            {
                userMiddleWare.Delete(id);
            }
            catch (Exception exception)
            {
                return Json(new { status = "error" });
            }
            return RedirectToAction("GetAll");
        }

    }
}