﻿
using System.ComponentModel.DataAnnotations;


namespace StockExchange.Models
{
    public class NewUser
    {
        [Required]
        [RegularExpression(@"^[A-Z][a-z]+$", ErrorMessage = "Field First name is invalid!")]
        [Display(Name = "First name: ")]
        public string FirstName { get; set; }
        [Required]
        [RegularExpression(@"^[A-Z][a-z]+$", ErrorMessage = "Field Last name is invalid!")]
        [Display(Name = "Last name: ")]
        public string LastName { get; set; }
        [Required]
        [EmailAddress(ErrorMessage = "Field Email is invalid!")]
        [Display(Name = "Email: ")]
        public string Email { get; set; }
        [Required]
        [Display(Name = "Address: ")]
        public string Address { get; set; }
        [Required]
        [Display(Name = "Password: ")]
        public string Password { get; set; }
    }
}
