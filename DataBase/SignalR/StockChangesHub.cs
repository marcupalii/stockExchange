﻿using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StockExchange.SignalR
{
    public class StockChangesHub : Hub
    {
        public Task UpdateStock(string user, string message)
        {
            var list_ = new List<string>() { user, message };
            return Clients.All.SendCoreAsync("UpdateStockClient", list_.ToArray());
        }
        public Task UpdateStockClient(string user, string message)
        {
            var list_ = new List<string>() { user, message };
            return Clients.All.SendCoreAsync("UpdateStockClient", list_.ToArray());
        }
    }
}
