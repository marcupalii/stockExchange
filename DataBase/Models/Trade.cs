﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;
using static Extensions.Extensions;

namespace StockExchange.DataGenerator.Models
{
    [DataContract]
    public class Trade
    {
        [DataMember]
        public Guid Id { get; set; }
        [DataMember]
        public Guid UserId { get; set; }
        [DataMember]
        public Guid StockId { get; set; }
        [DataMember]
        public float Price { get; set; }
        [DataMember]
        public OperationType Type { get; set; }
        [DataMember]
        public float Quantity { get; set; }
        [DataMember]
        public DateTime Date { get; set; }

        public Trade()
        {
            this.Id = Guid.NewGuid();
        }
    }
}
