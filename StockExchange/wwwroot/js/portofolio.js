﻿
$(document).ready(function () {
    "use strict";


    var connection = new signalR.HubConnectionBuilder().withUrl("/stockChangesHub").build();

    connection.on("UpdateStockClient", function (user, message) {
        var data = JSON.parse(message);

        $(`span#stock-price-${user}`).text(data.Price);

    });

    connection.start().then(function () {

    }).catch(function (err) {
        return console.error(err.toString());
    });




    $('#button-modal-new-stock').on("click", function () {
        window.location.href = `/Stock/Stocks/`;
    });


    // NewTopic, NewReview
    $('#button-save').on("click", function () {
        $('#general-form-add').submit();
    });
    /////////////////////////////////////

    $('#button-modal-new-portofolio').on("click", function () {
        $('#modal-title-edit-portofolio').text('Add new portofolio');
        $('#modal-edit-portofolio').load("/Portofolio/NewPortofolio");
        $('#modal-wrapper-edit-portofolio').modal("show");
    });

    $("#portofolio-input-filter").val($('#PortofolioValue').val()).trigger("change");

    $('#portofolio-input-filter').select2({ placeholder: "Select", theme: "classic" });
    //https://forums.asp.net/t/2154586.aspx?Select2+Multiple+Selected+Get+Data

    $('#portofolio-input-filter').on('change', function (e) {
        var id = $(this).val();
        window.location.href = `/Portofolio/Index/${id}`;

    });

    ////////////////////////////////////////
    var charts = [];

    $('.canvas-stock').each(function () {
        var id = (this).id;
        charts.push({
            key: id, //canva-guid
            value: new Chart(document.getElementById(id).getContext("2d"), {
                type: 'line',
                data: {
                    labels: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
                    datasets: [{
                        label: '#Last Week',
                        data: [0, 0, 0, 0, 0, 0, 0],
                        backgroundColor: [
                            'rgba(255, 99, 132, 0.2)'
                            //'rgba(54, 162, 235, 0.2)',
                            //'rgba(255, 206, 86, 0.2)',
                            //'rgba(75, 192, 192, 0.2)',
                            //'rgba(153, 102, 255, 0.2)',
                            //'rgba(255, 159, 64, 0.2)'
                        ],
                        borderColor: [
                            'rgba(255, 99, 132, 1)'
                            //    'rgba(54, 162, 235, 1)',
                            //    'rgba(255, 206, 86, 1)',
                            //    'rgba(75, 192, 192, 1)',
                            //    'rgba(153, 102, 255, 1)',
                            //    'rgba(255, 159, 64, 1)'
                        ],
                        borderWidth: 1
                    }]
                },
                options: {
                    responsive: true,
                    plugins: {
                        legend: {
                            position: 'top',
                        },
                        title: {
                            display: true,
                            text: 'Stock variation'
                        }
                    },
                    scales: {
                        x: {
                            display: true,
                            title: {
                                display: true,
                                text: 'Day'
                            }
                        },
                        y: {
                            display: true,
                            title: {
                                display: true,
                                text: 'Avg Stock Price'
                            }
                        }
                    }
                },
            })
        });

    });

    $('.trade-stock-button').on("click", function () {
        var id = $(this).prop("id");
        var portofolioId = $('#PortofolioValue').val();
        var price = $(`#stock-price-${id}`).text();

        $('#modal-title-edit-portofolio').text('New trade');
        $('#modal-edit-portofolio').load(`/Portofolio/${portofolioId}/Trade/${id}/${price}`);
        $('#modal-wrapper-edit-portofolio').modal("show");
    });

    var idPortofolio = $('#PortofolioValue').val();

    $.ajax({
        type: 'get',
        dataType: 'json',
        cache: false,
        url: `/Portofolio/StocksHistory/${idPortofolio}/`,
  
        success: function (response, textStatus, jqXHR) {
            var days = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
            for (var i = 0; i < response.length; i++) {
                var data = [];
                var labels = [];
                var item = response[i];
                var id = item.StockId;
                for (var j = 0; j < item.Values.length; j++) {
                    data.push(item.Values[j].Avg);
                    labels.push(days[item.Values[j].Day])
                }
                charts[i].value.data.labels = labels;
                charts[i].value.data.datasets[0].data = data;
                charts[i].value.update();
            }

            console.log(response);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert('Error - ' + errorThrown);
        }
    });

});

